import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Main {

    static int hiddenLayerSize = 20;
    //These Values are multiplied with 100 so we can use Integers to minimize Rounding errors
    static int minX = -1000; 
    static int maxX = 1000;
    static int step = 2;
    
    static double learnStep = 0.1;

    static List<Double> indices;
    static List<Double> trainingData;
    static Random rand;

    public static void main(String[] args) {
        //Seed the PRNG
        rand = new Random(42);

        //Calculate the value pairs
        populateTrainingData();
        
        //Build the MLP
        MLP mlp = new MLP(hiddenLayerSize);

        List<Double> errors = new ArrayList<>(); //The difference between the trainingdata and the netvalues
        List<Double> netvalues = new ArrayList<>(); //The values the net calculates

        //Run the net
        double totalError = runNet(mlp, errors, netvalues);

        //Store the errors (Aufgabe 1)
        storeErrors(netvalues, "Random");
        System.out.println(totalError);

        //Train the outputneuron (Run the net between every training to get the new errors)
        for (int i = 0; i < 10; i++) {
            mlp.trainOutputNeuron(errors);
            totalError = runNet(mlp, errors, netvalues);
            System.out.println(totalError);
        }

        //Store the values (Aufgabe 2)
        storeErrors(netvalues, "TrainOutput");
        System.out.println(totalError);

        //Train the hiddenlayer
        for (int i = 0; i < 100; i++) {
            List<Double> lastDeltas = mlp.trainOutputNeuron(errors);
            mlp.trainHiddenNeurons(errors, lastDeltas);
            totalError = runNet(mlp, errors, netvalues);
            System.out.println(totalError);
        }
        //Store the values (Aufgabe 3)
        storeErrors(netvalues, "TrainHidden");
        System.out.println(totalError);
    }

    //Stores the values to a file named <name>.txt
    public static void storeErrors(List<Double> values, String name) {
        try {
            FileWriter file = new FileWriter(name + ".txt");

            for (int i = 0; i < indices.size(); i++) {
                file.write(indices.get(i) + " " + values.get(i) + "\n");;
            }
            file.close();
        } catch (IOException e) {

        }
    }

    //Calculates the nets answer for every input
    public static double runNet(MLP mlp, List<Double> errors, List<Double> netvalues) {
        double totalError = 0.0;
        errors.clear();
        netvalues.clear();

        //Iterate through all inputs
        for (int i = 0; i < indices.size(); i++) {
            double netvalue = mlp.calc(indices.get(i)); //This is what the net calculates
            double error = trainingData.get(i) - netvalue; //This is the error
            totalError += Math.pow(error, 2); //Add the error squared to the total error
            errors.add(error);
            netvalues.add(netvalue);
        }
        //Average the totalError
        totalError /= indices.size();

        return totalError;
    }

    //Fills the trainingdata
    public static void populateTrainingData() {
        trainingData = new ArrayList<>();
        indices = new ArrayList<>();

        for (double x = minX; x <= maxX; x += step) {
            double doublex = x / 100; //Using integers instead of doubles minimizes rounding errors
            double y = Math.sin(doublex / 2) + Math.cos(1 / (Math.abs(doublex) + 0.1)) + 0.3 * doublex; 
            indices.add(doublex);
            trainingData.add(y);
        }
    }

    //Returns a random Double between -1.0 and 1.0
    public static double randomDoubleInterval() {
        return rand.nextDouble() * 2.0 - 1.0;
    }
}