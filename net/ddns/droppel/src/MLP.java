import java.util.ArrayList;
import java.util.List;

class MLP {
   
    //The MLPs layer of hidden Units
    private Unit[] hiddenUnits;
    private Unit outputUnit; //The MLPs output unit

    private int hiddenLayerSize;

    //The MLP gets passed the size of the hiddenlayer
    public MLP(int hiddenLayerSize) {
        this.hiddenLayerSize = hiddenLayerSize;

        hiddenUnits = new Unit[hiddenLayerSize];
        for (int i = 0; i < hiddenLayerSize; i++) {
            hiddenUnits[i] = new HiddenUnit(1);
        }

        outputUnit = new OutUnit(hiddenLayerSize);
    }


    //Calculate the MLPs output given an input
    public double calc(double in) {
        double[] hiddenLayerIn = new double[]{in}; //We need to convert the input to a list for the activate function
        double[] hiddenLayerOut = new double[hiddenLayerSize];

        //Activate each hidden neuron
        for (int i = 0; i < hiddenLayerSize; i++) {
            hiddenLayerOut[i] = hiddenUnits[i].activate(hiddenLayerIn);
        }

        //Return the output Units answer based on the values from the hidden layer
        return outputUnit.activate(hiddenLayerOut);
    }

    //Trains the Outputneuron
    public List<Double> trainOutputNeuron(List<Double> errors) {
        List<Double> lastDeltas = new ArrayList<>();

        double[] deltaValues = new double[hiddenLayerSize + 1];

        //Get the changes based on every error and add them together
        for (int i = 0; i < errors.size(); i++) {
            double[] singleDeltaValues = outputUnit.getDeltaValues(errors.get(i));
            for (int k = 0; k < deltaValues.length; k++) {
                deltaValues[k] += singleDeltaValues[k];
            }
            lastDeltas.add(outputUnit.lastDelta);
        }

        //Get the average for every suggested change
        for (int k = 0; k < deltaValues.length; k++) {
            deltaValues[k] /= errors.size();
        }

        //Change the weights
        for (int i = 0; i < hiddenLayerSize; i++) {
            outputUnit.changeWeight(i, deltaValues[i]);
        }
        outputUnit.changeBiasWeight(deltaValues[hiddenLayerSize]);

        return lastDeltas;
    }

    //Trains the Hiiden Units
    public void trainHiddenNeurons(List<Double> errors, List<Double> lastDeltas) {
        //Loop through all neurons
        for (int i = 0; i < hiddenLayerSize; i++) {
            double deltaValue = 0.0; //Change to the weight between "In" and the neuron
            double deltaValueBias = 0.0; //Change to the biasweight

            for (int e = 0; e < errors.size(); e++) {
                double[] singleDeltaValues = hiddenUnits[i].getDeltaValues(lastDeltas.get(e) * outputUnit.inWeights[i]);
                //In this case getDeltaValues always retuns 2 values, one for the weight between "In" and the neuron and one for the bias
                deltaValue += singleDeltaValues[0];
                deltaValueBias += singleDeltaValues[1];
            }

            //Average the weight changes
            deltaValue /= errors.size();
            deltaValueBias /= errors.size();

            //Apply the weight changes
            hiddenUnits[i].changeWeight(0, deltaValue);
            hiddenUnits[i].changeBiasWeight(deltaValueBias);
        }
    }

}