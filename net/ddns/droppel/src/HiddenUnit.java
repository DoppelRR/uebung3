public class HiddenUnit extends Unit{
    
    public HiddenUnit(int inSize) {
        super(inSize);
    }

    @Override
    //The Fermi function
    protected double activationFunction(double x) {
        return 1.0 / (1.0 + Math.exp(-1.0 * x));
    }


    @Override
    protected double activationFunctionDeriv(double x) {
        double fermi = activationFunction(x);
        return fermi * (1 - fermi);
    }
}
