class OutUnit extends Unit {

    public OutUnit(int inSize) {
        super(inSize);
    }

    @Override
    //For the output unit the activation function is linear
    protected double activationFunction(double x) {
        return x;
    }

    @Override
    protected double activationFunctionDeriv(double x) {
        return 1;
    }
}