import matplotlib.pyplot as plt
import numpy as np
import math 

def weirdfunction(x):
    return math.sin(x / 2) + math.cos(1 / (abs(x) + 0.1)) + 0.3 * x

fileRandom =open('Random.txt')
fileTrainOutput =open('TrainOutput.txt')
fileTrainHidden =open('TrainHidden.txt')

pairsRandom =  np.loadtxt(fileRandom, dtype = float , delimiter= ' ')
valuesRandom = np.array(pairsRandom[:,1].astype(float))

pairsTrainOutput = np.loadtxt(fileTrainOutput, dtype=float, delimiter=" ")
valuesTrainOutput = np.array(pairsTrainOutput[:,1].astype(float))

pairsTrainHidden = np.loadtxt(fileTrainHidden, dtype=float, delimiter=" ")
valuesTrainHidden = np.array(pairsTrainHidden[:,1].astype(float))

x = np.arange(-10,10.02,0.02)


weirdfunctionvector = np.vectorize(weirdfunction)
y = weirdfunctionvector(x)

plt.title('Eingabewerte der Funktion')
plt.xlabel('Schritte')
plt.ylabel('Werte')

plt.plot(x, y, '-', label="Function")

plt.plot(x, valuesRandom, label="Init with random weights")
plt.plot(x, valuesTrainOutput, label="After training Outputlayer")
plt.plot(x, valuesTrainHidden, label="After training Hiddenlayer")

plt.legend()

plt.show()

