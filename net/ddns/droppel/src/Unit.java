
//This class represents a Unit and all it's neccessary functions except for the activation function. THose are left to the child classes
abstract class Unit {

    private double biasWeight; //The biasweight
    public double[] inWeights; //The different input weights
    protected int inSize; //The amount of input units

    //Here we store different values used for the backpropagation
    protected double[] lastInputs; //All inputs
    protected double lastInput; //The sum of the last inputs multiplied by their weight "net_m"
    public double lastDelta; //The last delta

    //The Unit only needs to know its input size
    public Unit(int inSize) {
        this.inSize = inSize;
        this.biasWeight = Main.randomDoubleInterval(); //Set a random Bias weight between -1.0 and 1.0

        //Randomize all input weights between -1.0 and 1.0
        inWeights = new double[this.inSize];
        for (int i = 0; i < inSize; i++) {
            inWeights[i] = Main.randomDoubleInterval();
        }

        this.lastInputs = new double[inSize];
    }
   
    //Calculate the neurons output given a List of inputs
    public double activate(double[] input) {

        double sum = biasWeight; //Since the Bias is 1 * biasweight, we can initialize our sum with biasWeight

        //For every input multiply it with its corresponding weight and add it to sum
        for (int i = 0; i < inSize; i++) {
            double partialInput = inWeights[i] * input[i];
            sum += partialInput;
        }

        double output = activationFunction(sum);
        
        //Remember net_m and the last Inputs
        lastInput = sum;
        lastInputs = input;

        return output;
    }

    protected abstract double activationFunction(double x);
    protected abstract double activationFunctionDeriv(double x);

    //Returns the changes to the weights, based on a single value for its delta, this differs for different Unit Types
    //For the output unit this is the error
    //And for the hidden unit this is the delta value of the output unit times the weight connecting them
    public double[] getDeltaValues(double value) {
        double[] deltaValues = new double[inSize + 1]; //The changes to the weights

        double delta = value * activationFunctionDeriv(lastInput);  //Calculate this units delta
        lastDelta = delta;

        //For every weight calculate it's change
        for (int i = 0; i < inSize; i++) {
            deltaValues[i] = Main.learnStep * delta * lastInputs[i];
        }

        //Calculate delta Bias
        deltaValues[inSize] = Main.learnStep * delta;

        return deltaValues;
    }

    public void changeWeight(int i, double delta) {
        inWeights[i] += delta;
    }

    public void changeBiasWeight(double delta) {
        biasWeight += delta;
    }
}